var express = require('express');
var router = express.Router();

var Users = [
	{
		id: 1,
		firstName: 'Alex',
		lastName: 'Mischenko',
		username: 'alex',
		password: '123'
	},
	{
		id: 2,
		firstName: 'No',
		lastName: 'Name',
		username: 'test',
		password: 'test'
	},
	{
		id: 3,
		firstName: 'Qwe',
		lastName: 'Rty',
		username: 'qwer',
		password: 'qwer'
	},
];

router.post('/users/authenticate', function(req, res, next) {

	if(!req.body.username || !req.body.password){
		res.status("400");
		res.send("Invalid details!");
	} else {
		console.log('recived on POST /users/authenticate :');
		console.dir(req.body);

		userFound = Users.filter(user => {
			return user.username == req.body.username && user.password == req.body.password;
		});

		if (userFound.length){
			console.log(`user ${req.body.username} was found`);
			let user = userFound[0];
			let responseJson = {
				id: user.id,
				username: user.username,
				firstName: user.firstName,
				lastName: user.lastName,
				token: 'fake-jwt-token'
			};
			console.log(`sending JSON:`);
			console.dir(responseJson);
			res.json({ ok: true, user: responseJson });
		} 
		else 
		res.send('no such user found');
	}
});

module.exports = router;