
export const userService = {
	login,
	logout,
};

function login(username, password) {
	const requestOptions = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({ username, password })
	};

	return fetch(`api/users/authenticate`, requestOptions)
		.then(res => res.json())
		.then(data => {
			alert('in login() fetch then: ');
			alert(JSON.stringify(data));

			// 	// login successful if there's a jwt token in the response
			if (data.user.token) {
				// store user details and jwt token in local storage to keep user logged in between page refreshes
				localStorage.setItem('user', JSON.stringify(data.user));
			}

			return data.user;
		});		
}

function logout() {
	// remove user from local storage to log user out
	localStorage.removeItem('user');
}
