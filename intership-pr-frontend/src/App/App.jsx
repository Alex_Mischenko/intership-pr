import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../_helpers/history';
import { PrivateRoute } from '../_components/PrivateRoute';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';

import logo from './logo.svg';
import './App.css';

class App extends Component {
	render() {
	return (
		<div className="App">
		<header className="App-header">
			<img src={logo} className="App-logo" alt="logo" />
			<h1 className="App-title">Welcome to React</h1>
		</header>
		
		<Router history={history}>
			<div>
				<Switch>
					<Route exect path="/login" component={LoginPage} />
					<Route exect path="/register" render={() => <div><h2>Register</h2></div>} />
					<PrivateRoute exect path="/" component={HomePage} />
				</Switch>
			</div>
		</Router>

		</div>
	);
	}
}


function mapStateToProps(state) {
	return {
		...state
	};
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 