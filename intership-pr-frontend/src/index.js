import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './_helpers/store';

import './index.css';
import { App } from './App';
import registerServiceWorker from './registerServiceWorker';

// setup fake backend
// import { configureFakeBackend } from './_helpers/fake-backend';
// configureFakeBackend();

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>, 
	document.getElementById('root')
);
registerServiceWorker();
