import React from 'react';
import { NavLink } from 'react-router-dom'


export const HomePage = () => {
	return (
		<div>
			<p>
				<h1>Protected Home Page</h1>
			</p>
			<div>
				<NavLink to="/login">Logout</NavLink>
			</div>
		</div>
	);
}