import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'

import { authentication } from './authentication.reducer';
// import { registration } from './registration.reducer';

const rootReducer = combineReducers({
	authentication,
	form: formReducer
});

export default rootReducer;