import React from 'react';
import { connect } from 'react-redux';

import LoginForm  from './LoginForm';
import { userActions } from '../_actions';

class LoginPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			submitted: false
		}

		this.props.dispatch(userActions.logout());// log out on start up
	}

	submit = values => {
		this.setState({ submitted: true });// changing local state
		const { usernameInput, passwordInput } = values;
        const { dispatch } = this.props;
		if(usernameInput && passwordInput){
			dispatch(userActions.login(usernameInput, passwordInput));
		}
	}

	render() {
		return (
			<div>
				<h1>Login</h1>
				<LoginForm onSubmit={this.submit} />
			</div>
		);
	}
}

function mapStateToProps(state) {	
	return {
		...state
	};
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 
