import { userConstants } from '../_constants';
import { userService } from '../_services';
import { history } from '../_helpers';

export const userActions = {
	login,
	logout,
};

function login(username, password) {
	// alert(`login() recieved: username:${username}; password:${password}`);
	return dispatch => {
		dispatch(request({ username }));

		userService.login(username, password)
			.then(
				user => {
					alert("Login SUCCESS user:");
					alert(JSON.stringify(user));
					dispatch(success(user));
					history.push('/');
				},
				error => {
					alert("Login FAILURE error:");
					alert(JSON.stringify(error));
					dispatch(failure(error));
				}
			)

	}
	function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
	function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
	function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
	userService.logout();
	return { type: userConstants.LOGOUT }
}
